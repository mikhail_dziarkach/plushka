import React, { Component } from 'react';
import Layout from './components/Layout';
import './styles/main.scss';
import MainPage from './components/MainPage';
import { Switch, Route } from 'react-router-dom';
import SocialButtons from './components/SocialButtons';

export class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={MainPage} />
          <Route path="/cart" component={SocialButtons} />
          <Route path="/product/:id" component={MainPage} />
        </Switch>
      </Layout>
    );
  }
}
export default App;
