export const cartHeader = {
  title: 'Order',
  count: '23',
};

export const cartProductCard = {
  title: 'Название товара',
  src: require('../img/cat.jpg'),
  price: 1235,
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
  count: 5,
  link: '#',
};

export const socialButtons = [
  {
    label: 'vk',
    link: require('../img/socials-sprive.svg'),
  },
  {
    label: 'facebook',
    link: require('../img/socials-sprive.svg'),
  },
  {
    label: 'ok',
    link: require('../img/socials-sprive.svg'),
  },
  {
    label: 'youtube',
    link: require('../img/socials-sprive.svg'),
  },
];
