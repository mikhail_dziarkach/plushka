import React from 'react';
import SocialButtons from '../SocialButtons';
import NavMenu from '../NavMenu';
import navLinks from './constants';

import './styles.scss';
import Logo from '../Logo';

const Footer = props => {
  return (
    <footer className="footer">
      <div className="container m-size">
        <div className="footer__menu">
          <NavMenu list={navLinks} />
          <Logo />
          <NavMenu list={navLinks} />
          <SocialButtons {...props} />
        </div>
        <p className="footer__description">© 2010 — 2020 Privacy — Terms</p>
      </div>
    </footer>
  );
};

export default Footer;
