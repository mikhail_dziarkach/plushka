import React from 'react';
import './styles.scss';
import logo from './constants';
import { Link } from 'react-router-dom';

const Logo = () => (
  <Link to={logo.link} className="logo">
    {logo.logo}
  </Link>
);

export default Logo;
