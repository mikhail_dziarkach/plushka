import React from 'react';
import Logo from '../Logo';
import NavMenu from '../NavMenu';
import Button from '../Button';
import './styles.scss';
import navLinks from './constants';

const Header = () => {
  return (
    <header className="header">
      <Logo />
      <div className="header__navigation">
        <NavMenu list={navLinks} />
        <Button>Sign up</Button>
      </div>
    </header>
  );
};

export default Header;
