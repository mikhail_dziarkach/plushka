import React from 'react';
import Card from './index';

export default {
  title: 'Card',
  component: Card,
};

const props = {
  src: require('../../img/cat.jpg'),
  name: 'Some toy',
  price: 330,
};

export const CardBlock = () => <Card {...props} />;
