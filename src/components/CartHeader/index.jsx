import React from 'react';
import './styles.scss';
import PropTypes from 'prop-types';
import CartOderButton from '../CartButton';

const CartHeader = props => {
  return (
    <div className="cart">
      <div className="cart__title">
        <h1 className="cart__title_text">Basket</h1>
      </div>
      <div className="cart__buttons">
        <CartOderButton {...props} />
        <CartOderButton {...props} />
      </div>
    </div>
  );
};
CartHeader.propTypes = {
  props: {
    name: PropTypes.string.isRequired,
    count: PropTypes.number.isRequired,
    currency: PropTypes.bool.isRequired,
  },
};

export default CartHeader;
