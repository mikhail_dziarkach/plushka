import React from 'react';
import './styles.scss';
import welcomeData from './constant';

const Welcome = () => {
  return (
    <div className="welcome">
      <div className="container">
        <div className="welcome__media">
          <img src={welcomeData.src} alt={welcomeData.title} />
        </div>
        <div className="welcome__description">
          <h1 className="welcome__description-title">{welcomeData.title}</h1>
          <h2 className="welcome__description-sub-title">{welcomeData.subtitle}</h2>
          <p className="welcome__description-text">{welcomeData.text}</p>
          <div className="welcome__description-author">{welcomeData.author}</div>
        </div>
      </div>
    </div>
  );
};

// Welcome.defaultProps = {
//   link: require('./cat.jpg'),
// };

export default Welcome;
