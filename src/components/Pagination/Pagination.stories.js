import React from 'react';

import Pagination from './index';
import './styles.scss';

export default {
  component: Pagination,
  title: 'Pagination',
};

const paginationData = [
  {
    title: '1',
  },
  {
    title: '2',
  },
  {
    title: '3',
  },
  {
    title: '4',
  },
];

export const Default = () => {
  return <Pagination list={paginationData} />;
};
