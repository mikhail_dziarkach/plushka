import React from 'react';
import './styles.scss';
import PropTypes from 'prop-types';

const CartOderButton = ({ title, count }) => {
  return (
    <div className="cart__buttons-block">
      <p className="cart__buttons-block-title">{title}</p>
      <div className="cart__buttons-block-value">{count}</div>
    </div>
  );
};

CartOderButton.propTypes = {
  title: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
};

export default CartOderButton;
