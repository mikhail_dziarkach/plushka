import React from 'react';
import Card from '../Card';
import './ProductList.scss';
import productList from '../../mock/products';

const ProductList = () => {
  return (
    <div className="card-list">
      <div className="container flex">
        {productList.map(item => (
          <Card key={item.id} {...item} />
        ))}
      </div>
    </div>
  );
};

export default ProductList;
