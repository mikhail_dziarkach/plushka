import React, { Component } from 'react';
import Welcome from '../Welcome';
import FiltersBar from '../FiltersBar';
import ProductList from '../ProductList';
import Pagination from '../Pagination';

class MainPage extends Component {
  onChange() {}

  render() {
    return (
      <>
        <Welcome />
        <FiltersBar />
        <ProductList />
        <Pagination />
      </>
    );
  }
}

export default MainPage;
