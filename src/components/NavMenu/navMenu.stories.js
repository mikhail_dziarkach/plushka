import React from 'react';
import NavMenu from './index';
import { navMenu } from './constants.js';

export default {
  title: 'NavMenu',
  component: NavMenu,
};

export const Menu = () => <NavMenu list={navMenu} />;
