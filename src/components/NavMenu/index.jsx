import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import './styles.scss';

const NavMenu = ({ list }) => {
  return (
    <nav className="nav-menu">
      <ul className="nav-menu__list">
        {list.map(item => (
          <li key={item.title} className="nav-menu__list-item">
            <Link to={item.link} className="nav-menu__list-link">
              {item.title}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};

NavMenu.propTypes = {
  list: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      link: PropTypes.string.isRequired,
    })
  ).isRequired,
};

export default NavMenu;
