export const descriptionOfProduct = {
  src: require('../../img/cat.jpg'),
  title: 'Feature that is amazing',
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
  list: [
    {
      listTitle: 'some text 1',
    },
    {
      listTitle: 'some text 2',
    },
    {
      listTitle: 'some text 3',
    },
    {
      listTitle: 'some text 4',
    },
    {
      listTitle: 'some text 5',
    },
  ],
  price: 2000,
};
