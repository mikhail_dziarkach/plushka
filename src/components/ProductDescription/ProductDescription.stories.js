import React from 'react';
import ProductDescription from './index';
import { descriptionOfProduct } from './constants.js';

export default {
  title: 'ProductDescription',
  component: ProductDescription,
};

export const DescriptionProduct = () => <ProductDescription {...descriptionOfProduct} />;
