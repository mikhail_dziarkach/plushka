import React from 'react';
import Search from './index';

export default {
  title: 'Search',
  component: Search,
};

export const search = () => <Search />;
