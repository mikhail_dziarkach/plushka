import React from 'react';
import CartProductCard from './index';

export default {
  title: 'Cart',
  component: CartProductCard,
};
const props = {
  name: 'Название товара',
  src: require('../../img/cat.jpg'),
  price: 1235,
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
  count: 5,
  link: '#',
};

export const ProductCard = () => <CartProductCard {...props} />;
