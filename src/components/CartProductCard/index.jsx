import React from 'react';
import './styles.scss';
import PropTypes from 'prop-types';

const CartProductCard = ({ name, src, price, description, count, link }) => {
  return (
    <div className="basket-card basket-card_list">
      <div className="basket-card__image">
        {src && <img className="basket-card__image-item" src={src} alt={name} />}
      </div>
      <div className="basket-card__description">
        <div className="basket-card__description-title">{name}</div>
        {description && <div className="basket-card__description-content">{description}</div>}
        <div className="basket-card__description-button">
          <span className="basket-card__description-button-content">{price} $</span>
        </div>
        <div className="basket-card__description-container">
          <div className="basket-card__description-link">
            <a href={link} className="basket-card__description-link-action">
              Read More
            </a>
          </div>
          <div className="basket-card__description-amount">
            <span className="basket-card__description-amount-add">+</span>
            <span className="basket-card__description-amount-number">{count}</span>
            <span className="basket-card__description-amount-subtract">-</span>
          </div>
        </div>
      </div>
    </div>
  );
};
CartProductCard.propTypes = {
  name: PropTypes.string.isRequired,
  src: PropTypes.string,
  price: PropTypes.number.isRequired,
  description: PropTypes.string,
  count: PropTypes.number.isRequired,
  link: PropTypes.string.isRequired,
};

export default CartProductCard;
