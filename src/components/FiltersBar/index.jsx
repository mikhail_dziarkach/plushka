import React from 'react';
import Search from '../Search';
import './FilterBar.scss';
import Button from '../Button';
import NavMenu from '../NavMenu';
import navMenu from './constants';

const FiltersBar = () => {
  return (
    <div className="filters">
      <NavMenu list={navMenu} />
      <Search />
      <Button color="black">Search</Button>
    </div>
  );
};

export default FiltersBar;
