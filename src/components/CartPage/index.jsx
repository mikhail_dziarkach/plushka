import React, { Component } from 'react';
import CartHeader from '../CartHeader';
import CartProductCard from '../CartProductCard';
import SocialButtons from '../SocialButtons';

import { cartHeader, cartProductCard, socialButtons } from '../../mocks/mocks';

class CartPage extends Component {
  state = {
    cartHeader,
    cartProductCard,
    socialButtons,
  };

  render() {
    return (
      <>
        <CartHeader {...this.state.cartHeader} />
        <CartProductCard {...this.state.cartProductCard} />
        <SocialButtons list={this.state.socialButtons} />
      </>
    );
  }
}

export default CartPage;
