export const buttonColor = {
  black: 'black',
};

export const buttonType = {
  basket: 'basket',
  arrow: 'arrow',
  card: 'card',
};

export const buttonSize = {
  fullWidth: 'full',
};
