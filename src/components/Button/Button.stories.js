import React from 'react';
import { action } from '@storybook/addon-actions';
import { buttonColor, buttonType, buttonSize } from './constants';
import Button from './';

export default {
  title: 'Button',
  component: Button,
};

export const Default = () => <Button onClick={action('clicked')}>Button</Button>;
export const Black = () => (
  <Button color={buttonColor.black} onClick={action('clicked')}>
    Button
  </Button>
);

export const Basket = () => (
  <Button color={buttonColor.black} type={buttonType.basket} onClick={action('clicked')}>
    Button
  </Button>
);

export const FullWidth = () => (
  <Button size={buttonSize.fullWidth} onClick={action('clicked')}>
    Button
  </Button>
);

export const Arrow = () => (
  <Button type={buttonType.arrow} onClick={action('clicked')}>
    Button
  </Button>
);
